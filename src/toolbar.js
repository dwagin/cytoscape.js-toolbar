import defaults from './defaults'
import assign from './assign'

function Toolbar (cy, options) {
  this.cy = cy
  this.nodes = cy.collection()
  this.options = assign({}, defaults, options)

  let itemParams = typeof this.options.itemParams === typeof '' ? this.options.itemParams : this.options.itemParams()
  let items = []
  let shadows = []
  for (let i = 0; i < itemParams.length; i++) {
    let param = itemParams[i]
    let item = assign({}, param, {
      group: 'nodes',
      selectable: false,
      data: assign({}, param.data, {
        _toolbar: {
          position: assign({ x: 0, y: 0 }, param.position)
        }
      })
    })

    let shadow = {
      group: 'nodes',
      selectable: false,
      grabbable: false,
      data: {
        _toolbar: item.data._toolbar
      }
    }

    items.push(item)
    shadows.push(shadow)
  }

  cy.startBatch()
  let itemNodes = cy.add(items)
  let shadowNodes = cy.add(shadows)
  itemNodes.addClass('toolbar-item')
  shadowNodes.addClass('toolbar-item-shadow')
  this.nodes.merge(itemNodes).merge(shadowNodes)
  this.position()
  cy.endBatch()

  let bb = this.nodes.boundingBox()
  this.width = bb.w
  this.height = bb.h

  cy.on('viewport', () => {
    this.position()
  })

  itemNodes.on('free', (e) => {
    let { target, cy } = e
    let viewport = cy.extent()

    cy.startBatch()
    target.stop(true, true)
    if ((target.position().x - viewport.x1 - target.data('_toolbar').position.x) > this.width) {
      let nodeParams = options.nodeParams(target)
      let json = assign({}, nodeParams, { group: 'nodes' })
      let added = cy.add(json)
      let handler = this.options['complete']
      if (handler != null) handler(added)

      target.style('opacity', 0)
      target.animate({
        style: { 'opacity': 1 }, duration: 400, complete: () => {
          target.removeStyle('opacity')
        }
      })
    }
    target.position({
      x: viewport.x1 + target.data('_toolbar').position.x,
      y: viewport.y1 + target.data('_toolbar').position.y
    })
    cy.endBatch()
  })
}

let proto = Toolbar.prototype = {}
//let extend = obj => assign(proto, obj);

proto.position = function () {
  let { nodes, cy } = this
  let viewport = cy.extent()

  nodes.positions((node) => {
    return { x: viewport.x1 + node.data('_toolbar').position.x, y: viewport.y1 + node.data('_toolbar').position.y }
  })
}

export default Toolbar
