/* eslint-disable no-unused-vars */
const defaults = {
  itemParams: function (node) {
    return []
  },
  nodeParams: function (sourceNode) {
    // return element object to be passed to cy.add()
    return {}
  },
  complete: function (added) {
    // fired when element are added
  }
}
/* eslint-enable */

export default defaults
