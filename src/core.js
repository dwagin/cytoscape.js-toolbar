import Toolbar from './toolbar'

export default function (options) {
  let cy = this
  return new Toolbar(cy, options)
}
