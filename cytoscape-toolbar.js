(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else if(typeof exports === 'object')
		exports["cytoscapeToolbar"] = factory();
	else
		root["cytoscapeToolbar"] = factory();
})(window, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/assign.js":
/*!***********************!*\
  !*** ./src/assign.js ***!
  \***********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
// Simple, internal Object.assign() polyfill for options objects etc.
/* harmony default export */ __webpack_exports__["default"] = (Object.assign != null ? Object.assign.bind(Object) : function (t) {
  for (var _len = arguments.length, srcs = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
    srcs[_key - 1] = arguments[_key];
  }

  srcs.filter(function (src) {
    return src != null;
  }).forEach(function (src) {
    Object.keys(src).forEach(function (k) {
      return t[k] = src[k];
    });
  });
  return t;
});

/***/ }),

/***/ "./src/core.js":
/*!*********************!*\
  !*** ./src/core.js ***!
  \*********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _toolbar__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./toolbar */ "./src/toolbar.js");

/* harmony default export */ __webpack_exports__["default"] = (function (options) {
  var cy = this;
  return new _toolbar__WEBPACK_IMPORTED_MODULE_0__["default"](cy, options);
});

/***/ }),

/***/ "./src/defaults.js":
/*!*************************!*\
  !*** ./src/defaults.js ***!
  \*************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* eslint-disable no-unused-vars */
var defaults = {
  itemParams: function itemParams(node) {
    return [];
  },
  nodeParams: function nodeParams(sourceNode) {
    // return element object to be passed to cy.add()
    return {};
  },
  complete: function complete(added) {// fired when element are added
  }
  /* eslint-enable */

};
/* harmony default export */ __webpack_exports__["default"] = (defaults);

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./core */ "./src/core.js");
 // registers the extension on a cytoscape lib ref

var register = function register(cytoscape) {
  if (!cytoscape) {
    return;
  } // can't register if cytoscape unspecified


  cytoscape('core', 'toolbar', _core__WEBPACK_IMPORTED_MODULE_0__["default"]); // register with cytoscape.js
};

if (typeof cytoscape !== 'undefined') {
  // expose to global cytoscape (i.e. window.cytoscape)
  register(cytoscape); // eslint-disable-line no-undef
}

/* harmony default export */ __webpack_exports__["default"] = (register);

/***/ }),

/***/ "./src/toolbar.js":
/*!************************!*\
  !*** ./src/toolbar.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _defaults__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./defaults */ "./src/defaults.js");
/* harmony import */ var _assign__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./assign */ "./src/assign.js");
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }




function Toolbar(cy, options) {
  var _this = this;

  this.cy = cy;
  this.nodes = cy.collection();
  this.options = Object(_assign__WEBPACK_IMPORTED_MODULE_1__["default"])({}, _defaults__WEBPACK_IMPORTED_MODULE_0__["default"], options);
  var itemParams = _typeof(this.options.itemParams) === _typeof('') ? this.options.itemParams : this.options.itemParams();
  var items = [];
  var shadows = [];

  for (var i = 0; i < itemParams.length; i++) {
    var param = itemParams[i];
    var item = Object(_assign__WEBPACK_IMPORTED_MODULE_1__["default"])({}, param, {
      group: 'nodes',
      selectable: false,
      data: Object(_assign__WEBPACK_IMPORTED_MODULE_1__["default"])({}, param.data, {
        _toolbar: {
          position: Object(_assign__WEBPACK_IMPORTED_MODULE_1__["default"])({
            x: 0,
            y: 0
          }, param.position)
        }
      })
    });
    var shadow = {
      group: 'nodes',
      selectable: false,
      grabbable: false,
      data: {
        _toolbar: item.data._toolbar
      }
    };
    items.push(item);
    shadows.push(shadow);
  }

  cy.startBatch();
  var itemNodes = cy.add(items);
  var shadowNodes = cy.add(shadows);
  itemNodes.addClass('toolbar-item');
  shadowNodes.addClass('toolbar-item-shadow');
  this.nodes.merge(itemNodes).merge(shadowNodes);
  this.position();
  cy.endBatch();
  var bb = this.nodes.boundingBox();
  this.width = bb.w;
  this.height = bb.h;
  cy.on('viewport', function () {
    _this.position();
  });
  itemNodes.on('free', function (e) {
    var target = e.target,
        cy = e.cy;
    var viewport = cy.extent();
    cy.startBatch();
    target.stop(true, true);

    if (target.position().x - viewport.x1 - target.data('_toolbar').position.x > _this.width) {
      var nodeParams = options.nodeParams(target);
      var json = Object(_assign__WEBPACK_IMPORTED_MODULE_1__["default"])({}, nodeParams, {
        group: 'nodes'
      });
      var added = cy.add(json);
      var handler = _this.options['complete'];
      if (handler != null) handler(added);
      target.style('opacity', 0);
      target.animate({
        style: {
          'opacity': 1
        },
        duration: 400,
        complete: function complete() {
          target.removeStyle('opacity');
        }
      });
    }

    target.position({
      x: viewport.x1 + target.data('_toolbar').position.x,
      y: viewport.y1 + target.data('_toolbar').position.y
    });
    cy.endBatch();
  });
}

var proto = Toolbar.prototype = {}; //let extend = obj => assign(proto, obj);

proto.position = function () {
  var nodes = this.nodes,
      cy = this.cy;
  var viewport = cy.extent();
  nodes.positions(function (node) {
    return {
      x: viewport.x1 + node.data('_toolbar').position.x,
      y: viewport.y1 + node.data('_toolbar').position.y
    };
  });
};

/* harmony default export */ __webpack_exports__["default"] = (Toolbar);

/***/ })

/******/ });
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9jeXRvc2NhcGVUb29sYmFyL3dlYnBhY2svdW5pdmVyc2FsTW9kdWxlRGVmaW5pdGlvbiIsIndlYnBhY2s6Ly9jeXRvc2NhcGVUb29sYmFyL3dlYnBhY2svYm9vdHN0cmFwIiwid2VicGFjazovL2N5dG9zY2FwZVRvb2xiYXIvLi9zcmMvYXNzaWduLmpzIiwid2VicGFjazovL2N5dG9zY2FwZVRvb2xiYXIvLi9zcmMvY29yZS5qcyIsIndlYnBhY2s6Ly9jeXRvc2NhcGVUb29sYmFyLy4vc3JjL2RlZmF1bHRzLmpzIiwid2VicGFjazovL2N5dG9zY2FwZVRvb2xiYXIvLi9zcmMvaW5kZXguanMiLCJ3ZWJwYWNrOi8vY3l0b3NjYXBlVG9vbGJhci8uL3NyYy90b29sYmFyLmpzIl0sIm5hbWVzIjpbIk9iamVjdCIsImFzc2lnbiIsImJpbmQiLCJ0Iiwic3JjcyIsImZpbHRlciIsInNyYyIsImZvckVhY2giLCJrZXlzIiwiayIsIm9wdGlvbnMiLCJjeSIsIlRvb2xiYXIiLCJkZWZhdWx0cyIsIml0ZW1QYXJhbXMiLCJub2RlIiwibm9kZVBhcmFtcyIsInNvdXJjZU5vZGUiLCJjb21wbGV0ZSIsImFkZGVkIiwicmVnaXN0ZXIiLCJjeXRvc2NhcGUiLCJjb3JlIiwibm9kZXMiLCJjb2xsZWN0aW9uIiwiaXRlbXMiLCJzaGFkb3dzIiwiaSIsImxlbmd0aCIsInBhcmFtIiwiaXRlbSIsImdyb3VwIiwic2VsZWN0YWJsZSIsImRhdGEiLCJfdG9vbGJhciIsInBvc2l0aW9uIiwieCIsInkiLCJzaGFkb3ciLCJncmFiYmFibGUiLCJwdXNoIiwic3RhcnRCYXRjaCIsIml0ZW1Ob2RlcyIsImFkZCIsInNoYWRvd05vZGVzIiwiYWRkQ2xhc3MiLCJtZXJnZSIsImVuZEJhdGNoIiwiYmIiLCJib3VuZGluZ0JveCIsIndpZHRoIiwidyIsImhlaWdodCIsImgiLCJvbiIsImUiLCJ0YXJnZXQiLCJ2aWV3cG9ydCIsImV4dGVudCIsInN0b3AiLCJ4MSIsImpzb24iLCJoYW5kbGVyIiwic3R5bGUiLCJhbmltYXRlIiwiZHVyYXRpb24iLCJyZW1vdmVTdHlsZSIsInkxIiwicHJvdG8iLCJwcm90b3R5cGUiLCJwb3NpdGlvbnMiXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7QUFDRCxPO0FDVkE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxrREFBMEMsZ0NBQWdDO0FBQzFFO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsZ0VBQXdELGtCQUFrQjtBQUMxRTtBQUNBLHlEQUFpRCxjQUFjO0FBQy9EOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpREFBeUMsaUNBQWlDO0FBQzFFLHdIQUFnSCxtQkFBbUIsRUFBRTtBQUNySTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLG1DQUEyQiwwQkFBMEIsRUFBRTtBQUN2RCx5Q0FBaUMsZUFBZTtBQUNoRDtBQUNBO0FBQ0E7O0FBRUE7QUFDQSw4REFBc0QsK0RBQStEOztBQUVySDtBQUNBOzs7QUFHQTtBQUNBOzs7Ozs7Ozs7Ozs7O0FDbEZBO0FBQUE7QUFFZUEscUVBQU0sQ0FBQ0MsTUFBUCxJQUFpQixJQUFqQixHQUF3QkQsTUFBTSxDQUFDQyxNQUFQLENBQWNDLElBQWQsQ0FBbUJGLE1BQW5CLENBQXhCLEdBQXFELFVBQVVHLENBQVYsRUFBc0I7QUFBQSxvQ0FBTkMsSUFBTTtBQUFOQSxRQUFNO0FBQUE7O0FBQ3hGQSxNQUFJLENBQUNDLE1BQUwsQ0FBWSxVQUFBQyxHQUFHO0FBQUEsV0FBSUEsR0FBRyxJQUFJLElBQVg7QUFBQSxHQUFmLEVBQWdDQyxPQUFoQyxDQUF3QyxVQUFBRCxHQUFHLEVBQUk7QUFDN0NOLFVBQU0sQ0FBQ1EsSUFBUCxDQUFZRixHQUFaLEVBQWlCQyxPQUFqQixDQUF5QixVQUFBRSxDQUFDO0FBQUEsYUFBSU4sQ0FBQyxDQUFDTSxDQUFELENBQUQsR0FBT0gsR0FBRyxDQUFDRyxDQUFELENBQWQ7QUFBQSxLQUExQjtBQUNELEdBRkQ7QUFHQSxTQUFPTixDQUFQO0FBQ0QsQ0FMRCxFOzs7Ozs7Ozs7Ozs7QUNGQTtBQUFBO0FBQUE7QUFFZSx5RUFBVU8sT0FBVixFQUFtQjtBQUNoQyxNQUFJQyxFQUFFLEdBQUcsSUFBVDtBQUNBLFNBQU8sSUFBSUMsZ0RBQUosQ0FBWUQsRUFBWixFQUFnQkQsT0FBaEIsQ0FBUDtBQUNELEM7Ozs7Ozs7Ozs7OztBQ0xEO0FBQUE7QUFDQSxJQUFNRyxRQUFRLEdBQUc7QUFDZkMsWUFBVSxFQUFFLG9CQUFVQyxJQUFWLEVBQWdCO0FBQzFCLFdBQU8sRUFBUDtBQUNELEdBSGM7QUFJZkMsWUFBVSxFQUFFLG9CQUFVQyxVQUFWLEVBQXNCO0FBQ2hDO0FBQ0EsV0FBTyxFQUFQO0FBQ0QsR0FQYztBQVFmQyxVQUFRLEVBQUUsa0JBQVVDLEtBQVYsRUFBaUIsQ0FDekI7QUFDRDtBQUVIOztBQVppQixDQUFqQjtBQWNlTix1RUFBZixFOzs7Ozs7Ozs7Ozs7QUNmQTtBQUFBO0NBRUE7O0FBQ0EsSUFBSU8sUUFBUSxHQUFHLFNBQVhBLFFBQVcsQ0FBVUMsU0FBVixFQUFxQjtBQUNsQyxNQUFJLENBQUNBLFNBQUwsRUFBZ0I7QUFDZDtBQUNELEdBSGlDLENBR2hDOzs7QUFDRkEsV0FBUyxDQUFDLE1BQUQsRUFBUyxTQUFULEVBQW9CQyw2Q0FBcEIsQ0FBVCxDQUprQyxDQUlDO0FBQ3BDLENBTEQ7O0FBT0EsSUFBSSxPQUFPRCxTQUFQLEtBQXFCLFdBQXpCLEVBQXNDO0FBQUU7QUFDdENELFVBQVEsQ0FBQ0MsU0FBRCxDQUFSLENBRG9DLENBQ2hCO0FBQ3JCOztBQUVjRCx1RUFBZixFOzs7Ozs7Ozs7Ozs7Ozs7OztBQ2RBO0FBQ0E7O0FBRUEsU0FBU1IsT0FBVCxDQUFrQkQsRUFBbEIsRUFBc0JELE9BQXRCLEVBQStCO0FBQUE7O0FBQzdCLE9BQUtDLEVBQUwsR0FBVUEsRUFBVjtBQUNBLE9BQUtZLEtBQUwsR0FBYVosRUFBRSxDQUFDYSxVQUFILEVBQWI7QUFDQSxPQUFLZCxPQUFMLEdBQWVULHVEQUFNLENBQUMsRUFBRCxFQUFLWSxpREFBTCxFQUFlSCxPQUFmLENBQXJCO0FBRUEsTUFBSUksVUFBVSxHQUFHLFFBQU8sS0FBS0osT0FBTCxDQUFhSSxVQUFwQixjQUEwQyxFQUExQyxJQUErQyxLQUFLSixPQUFMLENBQWFJLFVBQTVELEdBQXlFLEtBQUtKLE9BQUwsQ0FBYUksVUFBYixFQUExRjtBQUNBLE1BQUlXLEtBQUssR0FBRyxFQUFaO0FBQ0EsTUFBSUMsT0FBTyxHQUFHLEVBQWQ7O0FBQ0EsT0FBSyxJQUFJQyxDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHYixVQUFVLENBQUNjLE1BQS9CLEVBQXVDRCxDQUFDLEVBQXhDLEVBQTRDO0FBQzFDLFFBQUlFLEtBQUssR0FBR2YsVUFBVSxDQUFDYSxDQUFELENBQXRCO0FBQ0EsUUFBSUcsSUFBSSxHQUFHN0IsdURBQU0sQ0FBQyxFQUFELEVBQUs0QixLQUFMLEVBQVk7QUFDM0JFLFdBQUssRUFBRSxPQURvQjtBQUUzQkMsZ0JBQVUsRUFBRSxLQUZlO0FBRzNCQyxVQUFJLEVBQUVoQyx1REFBTSxDQUFDLEVBQUQsRUFBSzRCLEtBQUssQ0FBQ0ksSUFBWCxFQUFpQjtBQUMzQkMsZ0JBQVEsRUFBRTtBQUNSQyxrQkFBUSxFQUFFbEMsdURBQU0sQ0FBQztBQUFFbUMsYUFBQyxFQUFFLENBQUw7QUFBUUMsYUFBQyxFQUFFO0FBQVgsV0FBRCxFQUFpQlIsS0FBSyxDQUFDTSxRQUF2QjtBQURSO0FBRGlCLE9BQWpCO0FBSGUsS0FBWixDQUFqQjtBQVVBLFFBQUlHLE1BQU0sR0FBRztBQUNYUCxXQUFLLEVBQUUsT0FESTtBQUVYQyxnQkFBVSxFQUFFLEtBRkQ7QUFHWE8sZUFBUyxFQUFFLEtBSEE7QUFJWE4sVUFBSSxFQUFFO0FBQ0pDLGdCQUFRLEVBQUVKLElBQUksQ0FBQ0csSUFBTCxDQUFVQztBQURoQjtBQUpLLEtBQWI7QUFTQVQsU0FBSyxDQUFDZSxJQUFOLENBQVdWLElBQVg7QUFDQUosV0FBTyxDQUFDYyxJQUFSLENBQWFGLE1BQWI7QUFDRDs7QUFFRDNCLElBQUUsQ0FBQzhCLFVBQUg7QUFDQSxNQUFJQyxTQUFTLEdBQUcvQixFQUFFLENBQUNnQyxHQUFILENBQU9sQixLQUFQLENBQWhCO0FBQ0EsTUFBSW1CLFdBQVcsR0FBR2pDLEVBQUUsQ0FBQ2dDLEdBQUgsQ0FBT2pCLE9BQVAsQ0FBbEI7QUFDQWdCLFdBQVMsQ0FBQ0csUUFBVixDQUFtQixjQUFuQjtBQUNBRCxhQUFXLENBQUNDLFFBQVosQ0FBcUIscUJBQXJCO0FBQ0EsT0FBS3RCLEtBQUwsQ0FBV3VCLEtBQVgsQ0FBaUJKLFNBQWpCLEVBQTRCSSxLQUE1QixDQUFrQ0YsV0FBbEM7QUFDQSxPQUFLVCxRQUFMO0FBQ0F4QixJQUFFLENBQUNvQyxRQUFIO0FBRUEsTUFBSUMsRUFBRSxHQUFHLEtBQUt6QixLQUFMLENBQVcwQixXQUFYLEVBQVQ7QUFDQSxPQUFLQyxLQUFMLEdBQWFGLEVBQUUsQ0FBQ0csQ0FBaEI7QUFDQSxPQUFLQyxNQUFMLEdBQWNKLEVBQUUsQ0FBQ0ssQ0FBakI7QUFFQTFDLElBQUUsQ0FBQzJDLEVBQUgsQ0FBTSxVQUFOLEVBQWtCLFlBQU07QUFDdEIsU0FBSSxDQUFDbkIsUUFBTDtBQUNELEdBRkQ7QUFJQU8sV0FBUyxDQUFDWSxFQUFWLENBQWEsTUFBYixFQUFxQixVQUFDQyxDQUFELEVBQU87QUFBQSxRQUNwQkMsTUFEb0IsR0FDTEQsQ0FESyxDQUNwQkMsTUFEb0I7QUFBQSxRQUNaN0MsRUFEWSxHQUNMNEMsQ0FESyxDQUNaNUMsRUFEWTtBQUUxQixRQUFJOEMsUUFBUSxHQUFHOUMsRUFBRSxDQUFDK0MsTUFBSCxFQUFmO0FBRUEvQyxNQUFFLENBQUM4QixVQUFIO0FBQ0FlLFVBQU0sQ0FBQ0csSUFBUCxDQUFZLElBQVosRUFBa0IsSUFBbEI7O0FBQ0EsUUFBS0gsTUFBTSxDQUFDckIsUUFBUCxHQUFrQkMsQ0FBbEIsR0FBc0JxQixRQUFRLENBQUNHLEVBQS9CLEdBQW9DSixNQUFNLENBQUN2QixJQUFQLENBQVksVUFBWixFQUF3QkUsUUFBeEIsQ0FBaUNDLENBQXRFLEdBQTJFLEtBQUksQ0FBQ2MsS0FBcEYsRUFBMkY7QUFDekYsVUFBSWxDLFVBQVUsR0FBR04sT0FBTyxDQUFDTSxVQUFSLENBQW1Cd0MsTUFBbkIsQ0FBakI7QUFDQSxVQUFJSyxJQUFJLEdBQUc1RCx1REFBTSxDQUFDLEVBQUQsRUFBS2UsVUFBTCxFQUFpQjtBQUFFZSxhQUFLLEVBQUU7QUFBVCxPQUFqQixDQUFqQjtBQUNBLFVBQUlaLEtBQUssR0FBR1IsRUFBRSxDQUFDZ0MsR0FBSCxDQUFPa0IsSUFBUCxDQUFaO0FBQ0EsVUFBSUMsT0FBTyxHQUFHLEtBQUksQ0FBQ3BELE9BQUwsQ0FBYSxVQUFiLENBQWQ7QUFDQSxVQUFJb0QsT0FBTyxJQUFJLElBQWYsRUFBcUJBLE9BQU8sQ0FBQzNDLEtBQUQsQ0FBUDtBQUVyQnFDLFlBQU0sQ0FBQ08sS0FBUCxDQUFhLFNBQWIsRUFBd0IsQ0FBeEI7QUFDQVAsWUFBTSxDQUFDUSxPQUFQLENBQWU7QUFDYkQsYUFBSyxFQUFFO0FBQUUscUJBQVc7QUFBYixTQURNO0FBQ1lFLGdCQUFRLEVBQUUsR0FEdEI7QUFDMkIvQyxnQkFBUSxFQUFFLG9CQUFNO0FBQ3REc0MsZ0JBQU0sQ0FBQ1UsV0FBUCxDQUFtQixTQUFuQjtBQUNEO0FBSFksT0FBZjtBQUtEOztBQUNEVixVQUFNLENBQUNyQixRQUFQLENBQWdCO0FBQ2RDLE9BQUMsRUFBRXFCLFFBQVEsQ0FBQ0csRUFBVCxHQUFjSixNQUFNLENBQUN2QixJQUFQLENBQVksVUFBWixFQUF3QkUsUUFBeEIsQ0FBaUNDLENBRHBDO0FBRWRDLE9BQUMsRUFBRW9CLFFBQVEsQ0FBQ1UsRUFBVCxHQUFjWCxNQUFNLENBQUN2QixJQUFQLENBQVksVUFBWixFQUF3QkUsUUFBeEIsQ0FBaUNFO0FBRnBDLEtBQWhCO0FBSUExQixNQUFFLENBQUNvQyxRQUFIO0FBQ0QsR0F6QkQ7QUEwQkQ7O0FBRUQsSUFBSXFCLEtBQUssR0FBR3hELE9BQU8sQ0FBQ3lELFNBQVIsR0FBb0IsRUFBaEMsQyxDQUNBOztBQUVBRCxLQUFLLENBQUNqQyxRQUFOLEdBQWlCLFlBQVk7QUFBQSxNQUNyQlosS0FEcUIsR0FDUCxJQURPLENBQ3JCQSxLQURxQjtBQUFBLE1BQ2RaLEVBRGMsR0FDUCxJQURPLENBQ2RBLEVBRGM7QUFFM0IsTUFBSThDLFFBQVEsR0FBRzlDLEVBQUUsQ0FBQytDLE1BQUgsRUFBZjtBQUVBbkMsT0FBSyxDQUFDK0MsU0FBTixDQUFnQixVQUFDdkQsSUFBRCxFQUFVO0FBQ3hCLFdBQU87QUFBRXFCLE9BQUMsRUFBRXFCLFFBQVEsQ0FBQ0csRUFBVCxHQUFjN0MsSUFBSSxDQUFDa0IsSUFBTCxDQUFVLFVBQVYsRUFBc0JFLFFBQXRCLENBQStCQyxDQUFsRDtBQUFxREMsT0FBQyxFQUFFb0IsUUFBUSxDQUFDVSxFQUFULEdBQWNwRCxJQUFJLENBQUNrQixJQUFMLENBQVUsVUFBVixFQUFzQkUsUUFBdEIsQ0FBK0JFO0FBQXJHLEtBQVA7QUFDRCxHQUZEO0FBR0QsQ0FQRDs7QUFTZXpCLHNFQUFmLEUiLCJmaWxlIjoiY3l0b3NjYXBlLXRvb2xiYXIuanMiLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24gd2VicGFja1VuaXZlcnNhbE1vZHVsZURlZmluaXRpb24ocm9vdCwgZmFjdG9yeSkge1xuXHRpZih0eXBlb2YgZXhwb3J0cyA9PT0gJ29iamVjdCcgJiYgdHlwZW9mIG1vZHVsZSA9PT0gJ29iamVjdCcpXG5cdFx0bW9kdWxlLmV4cG9ydHMgPSBmYWN0b3J5KCk7XG5cdGVsc2UgaWYodHlwZW9mIGRlZmluZSA9PT0gJ2Z1bmN0aW9uJyAmJiBkZWZpbmUuYW1kKVxuXHRcdGRlZmluZShbXSwgZmFjdG9yeSk7XG5cdGVsc2UgaWYodHlwZW9mIGV4cG9ydHMgPT09ICdvYmplY3QnKVxuXHRcdGV4cG9ydHNbXCJjeXRvc2NhcGVUb29sYmFyXCJdID0gZmFjdG9yeSgpO1xuXHRlbHNlXG5cdFx0cm9vdFtcImN5dG9zY2FwZVRvb2xiYXJcIl0gPSBmYWN0b3J5KCk7XG59KSh3aW5kb3csIGZ1bmN0aW9uKCkge1xucmV0dXJuICIsIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwgeyBlbnVtZXJhYmxlOiB0cnVlLCBnZXQ6IGdldHRlciB9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZGVmaW5lIF9fZXNNb2R1bGUgb24gZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yID0gZnVuY3Rpb24oZXhwb3J0cykge1xuIFx0XHRpZih0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJiBTeW1ib2wudG9TdHJpbmdUYWcpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgU3ltYm9sLnRvU3RyaW5nVGFnLCB7IHZhbHVlOiAnTW9kdWxlJyB9KTtcbiBcdFx0fVxuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xuIFx0fTtcblxuIFx0Ly8gY3JlYXRlIGEgZmFrZSBuYW1lc3BhY2Ugb2JqZWN0XG4gXHQvLyBtb2RlICYgMTogdmFsdWUgaXMgYSBtb2R1bGUgaWQsIHJlcXVpcmUgaXRcbiBcdC8vIG1vZGUgJiAyOiBtZXJnZSBhbGwgcHJvcGVydGllcyBvZiB2YWx1ZSBpbnRvIHRoZSBuc1xuIFx0Ly8gbW9kZSAmIDQ6IHJldHVybiB2YWx1ZSB3aGVuIGFscmVhZHkgbnMgb2JqZWN0XG4gXHQvLyBtb2RlICYgOHwxOiBiZWhhdmUgbGlrZSByZXF1aXJlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnQgPSBmdW5jdGlvbih2YWx1ZSwgbW9kZSkge1xuIFx0XHRpZihtb2RlICYgMSkgdmFsdWUgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKHZhbHVlKTtcbiBcdFx0aWYobW9kZSAmIDgpIHJldHVybiB2YWx1ZTtcbiBcdFx0aWYoKG1vZGUgJiA0KSAmJiB0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnICYmIHZhbHVlICYmIHZhbHVlLl9fZXNNb2R1bGUpIHJldHVybiB2YWx1ZTtcbiBcdFx0dmFyIG5zID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yKG5zKTtcbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KG5zLCAnZGVmYXVsdCcsIHsgZW51bWVyYWJsZTogdHJ1ZSwgdmFsdWU6IHZhbHVlIH0pO1xuIFx0XHRpZihtb2RlICYgMiAmJiB0eXBlb2YgdmFsdWUgIT0gJ3N0cmluZycpIGZvcih2YXIga2V5IGluIHZhbHVlKSBfX3dlYnBhY2tfcmVxdWlyZV9fLmQobnMsIGtleSwgZnVuY3Rpb24oa2V5KSB7IHJldHVybiB2YWx1ZVtrZXldOyB9LmJpbmQobnVsbCwga2V5KSk7XG4gXHRcdHJldHVybiBucztcbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSBcIi4vc3JjL2luZGV4LmpzXCIpO1xuIiwiLy8gU2ltcGxlLCBpbnRlcm5hbCBPYmplY3QuYXNzaWduKCkgcG9seWZpbGwgZm9yIG9wdGlvbnMgb2JqZWN0cyBldGMuXG5cbmV4cG9ydCBkZWZhdWx0IE9iamVjdC5hc3NpZ24gIT0gbnVsbCA/IE9iamVjdC5hc3NpZ24uYmluZChPYmplY3QpIDogZnVuY3Rpb24gKHQsIC4uLnNyY3MpIHtcbiAgc3Jjcy5maWx0ZXIoc3JjID0+IHNyYyAhPSBudWxsKS5mb3JFYWNoKHNyYyA9PiB7XG4gICAgT2JqZWN0LmtleXMoc3JjKS5mb3JFYWNoKGsgPT4gdFtrXSA9IHNyY1trXSlcbiAgfSlcbiAgcmV0dXJuIHRcbn1cbiIsImltcG9ydCBUb29sYmFyIGZyb20gJy4vdG9vbGJhcidcblxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gKG9wdGlvbnMpIHtcbiAgbGV0IGN5ID0gdGhpc1xuICByZXR1cm4gbmV3IFRvb2xiYXIoY3ksIG9wdGlvbnMpXG59XG4iLCIvKiBlc2xpbnQtZGlzYWJsZSBuby11bnVzZWQtdmFycyAqL1xuY29uc3QgZGVmYXVsdHMgPSB7XG4gIGl0ZW1QYXJhbXM6IGZ1bmN0aW9uIChub2RlKSB7XG4gICAgcmV0dXJuIFtdXG4gIH0sXG4gIG5vZGVQYXJhbXM6IGZ1bmN0aW9uIChzb3VyY2VOb2RlKSB7XG4gICAgLy8gcmV0dXJuIGVsZW1lbnQgb2JqZWN0IHRvIGJlIHBhc3NlZCB0byBjeS5hZGQoKVxuICAgIHJldHVybiB7fVxuICB9LFxuICBjb21wbGV0ZTogZnVuY3Rpb24gKGFkZGVkKSB7XG4gICAgLy8gZmlyZWQgd2hlbiBlbGVtZW50IGFyZSBhZGRlZFxuICB9XG59XG4vKiBlc2xpbnQtZW5hYmxlICovXG5cbmV4cG9ydCBkZWZhdWx0IGRlZmF1bHRzXG4iLCJpbXBvcnQgY29yZSBmcm9tICcuL2NvcmUnXG5cbi8vIHJlZ2lzdGVycyB0aGUgZXh0ZW5zaW9uIG9uIGEgY3l0b3NjYXBlIGxpYiByZWZcbmxldCByZWdpc3RlciA9IGZ1bmN0aW9uIChjeXRvc2NhcGUpIHtcbiAgaWYgKCFjeXRvc2NhcGUpIHtcbiAgICByZXR1cm5cbiAgfSAvLyBjYW4ndCByZWdpc3RlciBpZiBjeXRvc2NhcGUgdW5zcGVjaWZpZWRcbiAgY3l0b3NjYXBlKCdjb3JlJywgJ3Rvb2xiYXInLCBjb3JlKSAvLyByZWdpc3RlciB3aXRoIGN5dG9zY2FwZS5qc1xufVxuXG5pZiAodHlwZW9mIGN5dG9zY2FwZSAhPT0gJ3VuZGVmaW5lZCcpIHsgLy8gZXhwb3NlIHRvIGdsb2JhbCBjeXRvc2NhcGUgKGkuZS4gd2luZG93LmN5dG9zY2FwZSlcbiAgcmVnaXN0ZXIoY3l0b3NjYXBlKSAvLyBlc2xpbnQtZGlzYWJsZS1saW5lIG5vLXVuZGVmXG59XG5cbmV4cG9ydCBkZWZhdWx0IHJlZ2lzdGVyXG4iLCJpbXBvcnQgZGVmYXVsdHMgZnJvbSAnLi9kZWZhdWx0cydcbmltcG9ydCBhc3NpZ24gZnJvbSAnLi9hc3NpZ24nXG5cbmZ1bmN0aW9uIFRvb2xiYXIgKGN5LCBvcHRpb25zKSB7XG4gIHRoaXMuY3kgPSBjeVxuICB0aGlzLm5vZGVzID0gY3kuY29sbGVjdGlvbigpXG4gIHRoaXMub3B0aW9ucyA9IGFzc2lnbih7fSwgZGVmYXVsdHMsIG9wdGlvbnMpXG5cbiAgbGV0IGl0ZW1QYXJhbXMgPSB0eXBlb2YgdGhpcy5vcHRpb25zLml0ZW1QYXJhbXMgPT09IHR5cGVvZiAnJyA/IHRoaXMub3B0aW9ucy5pdGVtUGFyYW1zIDogdGhpcy5vcHRpb25zLml0ZW1QYXJhbXMoKVxuICBsZXQgaXRlbXMgPSBbXVxuICBsZXQgc2hhZG93cyA9IFtdXG4gIGZvciAobGV0IGkgPSAwOyBpIDwgaXRlbVBhcmFtcy5sZW5ndGg7IGkrKykge1xuICAgIGxldCBwYXJhbSA9IGl0ZW1QYXJhbXNbaV1cbiAgICBsZXQgaXRlbSA9IGFzc2lnbih7fSwgcGFyYW0sIHtcbiAgICAgIGdyb3VwOiAnbm9kZXMnLFxuICAgICAgc2VsZWN0YWJsZTogZmFsc2UsXG4gICAgICBkYXRhOiBhc3NpZ24oe30sIHBhcmFtLmRhdGEsIHtcbiAgICAgICAgX3Rvb2xiYXI6IHtcbiAgICAgICAgICBwb3NpdGlvbjogYXNzaWduKHsgeDogMCwgeTogMCB9LCBwYXJhbS5wb3NpdGlvbilcbiAgICAgICAgfVxuICAgICAgfSlcbiAgICB9KVxuXG4gICAgbGV0IHNoYWRvdyA9IHtcbiAgICAgIGdyb3VwOiAnbm9kZXMnLFxuICAgICAgc2VsZWN0YWJsZTogZmFsc2UsXG4gICAgICBncmFiYmFibGU6IGZhbHNlLFxuICAgICAgZGF0YToge1xuICAgICAgICBfdG9vbGJhcjogaXRlbS5kYXRhLl90b29sYmFyXG4gICAgICB9XG4gICAgfVxuXG4gICAgaXRlbXMucHVzaChpdGVtKVxuICAgIHNoYWRvd3MucHVzaChzaGFkb3cpXG4gIH1cblxuICBjeS5zdGFydEJhdGNoKClcbiAgbGV0IGl0ZW1Ob2RlcyA9IGN5LmFkZChpdGVtcylcbiAgbGV0IHNoYWRvd05vZGVzID0gY3kuYWRkKHNoYWRvd3MpXG4gIGl0ZW1Ob2Rlcy5hZGRDbGFzcygndG9vbGJhci1pdGVtJylcbiAgc2hhZG93Tm9kZXMuYWRkQ2xhc3MoJ3Rvb2xiYXItaXRlbS1zaGFkb3cnKVxuICB0aGlzLm5vZGVzLm1lcmdlKGl0ZW1Ob2RlcykubWVyZ2Uoc2hhZG93Tm9kZXMpXG4gIHRoaXMucG9zaXRpb24oKVxuICBjeS5lbmRCYXRjaCgpXG5cbiAgbGV0IGJiID0gdGhpcy5ub2Rlcy5ib3VuZGluZ0JveCgpXG4gIHRoaXMud2lkdGggPSBiYi53XG4gIHRoaXMuaGVpZ2h0ID0gYmIuaFxuXG4gIGN5Lm9uKCd2aWV3cG9ydCcsICgpID0+IHtcbiAgICB0aGlzLnBvc2l0aW9uKClcbiAgfSlcblxuICBpdGVtTm9kZXMub24oJ2ZyZWUnLCAoZSkgPT4ge1xuICAgIGxldCB7IHRhcmdldCwgY3kgfSA9IGVcbiAgICBsZXQgdmlld3BvcnQgPSBjeS5leHRlbnQoKVxuXG4gICAgY3kuc3RhcnRCYXRjaCgpXG4gICAgdGFyZ2V0LnN0b3AodHJ1ZSwgdHJ1ZSlcbiAgICBpZiAoKHRhcmdldC5wb3NpdGlvbigpLnggLSB2aWV3cG9ydC54MSAtIHRhcmdldC5kYXRhKCdfdG9vbGJhcicpLnBvc2l0aW9uLngpID4gdGhpcy53aWR0aCkge1xuICAgICAgbGV0IG5vZGVQYXJhbXMgPSBvcHRpb25zLm5vZGVQYXJhbXModGFyZ2V0KVxuICAgICAgbGV0IGpzb24gPSBhc3NpZ24oe30sIG5vZGVQYXJhbXMsIHsgZ3JvdXA6ICdub2RlcycgfSlcbiAgICAgIGxldCBhZGRlZCA9IGN5LmFkZChqc29uKVxuICAgICAgbGV0IGhhbmRsZXIgPSB0aGlzLm9wdGlvbnNbJ2NvbXBsZXRlJ11cbiAgICAgIGlmIChoYW5kbGVyICE9IG51bGwpIGhhbmRsZXIoYWRkZWQpXG5cbiAgICAgIHRhcmdldC5zdHlsZSgnb3BhY2l0eScsIDApXG4gICAgICB0YXJnZXQuYW5pbWF0ZSh7XG4gICAgICAgIHN0eWxlOiB7ICdvcGFjaXR5JzogMSB9LCBkdXJhdGlvbjogNDAwLCBjb21wbGV0ZTogKCkgPT4ge1xuICAgICAgICAgIHRhcmdldC5yZW1vdmVTdHlsZSgnb3BhY2l0eScpXG4gICAgICAgIH1cbiAgICAgIH0pXG4gICAgfVxuICAgIHRhcmdldC5wb3NpdGlvbih7XG4gICAgICB4OiB2aWV3cG9ydC54MSArIHRhcmdldC5kYXRhKCdfdG9vbGJhcicpLnBvc2l0aW9uLngsXG4gICAgICB5OiB2aWV3cG9ydC55MSArIHRhcmdldC5kYXRhKCdfdG9vbGJhcicpLnBvc2l0aW9uLnlcbiAgICB9KVxuICAgIGN5LmVuZEJhdGNoKClcbiAgfSlcbn1cblxubGV0IHByb3RvID0gVG9vbGJhci5wcm90b3R5cGUgPSB7fVxuLy9sZXQgZXh0ZW5kID0gb2JqID0+IGFzc2lnbihwcm90bywgb2JqKTtcblxucHJvdG8ucG9zaXRpb24gPSBmdW5jdGlvbiAoKSB7XG4gIGxldCB7IG5vZGVzLCBjeSB9ID0gdGhpc1xuICBsZXQgdmlld3BvcnQgPSBjeS5leHRlbnQoKVxuXG4gIG5vZGVzLnBvc2l0aW9ucygobm9kZSkgPT4ge1xuICAgIHJldHVybiB7IHg6IHZpZXdwb3J0LngxICsgbm9kZS5kYXRhKCdfdG9vbGJhcicpLnBvc2l0aW9uLngsIHk6IHZpZXdwb3J0LnkxICsgbm9kZS5kYXRhKCdfdG9vbGJhcicpLnBvc2l0aW9uLnkgfVxuICB9KVxufVxuXG5leHBvcnQgZGVmYXVsdCBUb29sYmFyXG4iXSwic291cmNlUm9vdCI6IiJ9